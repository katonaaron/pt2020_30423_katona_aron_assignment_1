\documentclass[a4paper,10pt]{article}
\usepackage{fontspec}
\usepackage[singlespacing]{setspace}
\usepackage{listings}
\usepackage{color}
\usepackage{graphicx}
\usepackage{hyperref}
\hypersetup{
    colorlinks,
    citecolor=black,
    filecolor=black,
    linkcolor=black,
    urlcolor=black
}
\setmainfont{Times New Roman}

%opening
\title{Polynomial Calculator}
\author{Katona Áron}

\begin{document}

    \pagenumbering{gobble}
    \maketitle
    \newpage
    \pagenumbering{arabic}
    \tableofcontents
    \newpage


    \section{Objective}

    \paragraph{}
    The main objective of the assignment is to design and implement a polynomial calculator with a dedicated graphical interface through which the user can enter polynomials, select the operation to be performed and display the result. \par
    The supported operations are: addition, subtraction,
    multiplication, division, differentiation and integration.\par
    The polynomials have integer coefficients and only one variable.

    \subsection{Secondary objectives}
    The main objective can be divided into multiple independent steps:
    \begin{itemize}
        \item \textbf{Creation of a graphical user interface} \hyperref[subsec:view]{[\ref{subsec:view}]}
        \item \textbf{Designing the models which would store the polynomials} \hyperref[subsec:model]{[\ref{subsec:model}]}
        \item \textbf{Implementation of the mathematical operations} \hyperref[subsubsec:monomials]{[\ref{subsubsec:monomials}]}
        \item \textbf{User input validation} \hyperref[subsubsec:valid]{[\ref{subsubsec:valid}]}
        \item \textbf{Intercommunication between components} \hyperref[subsubsec:op_serv]{[\ref{subsubsec:op_serv}]}
    \end{itemize}


    \section{Problem analysis and use cases}

    \subsection{Problem analysis}
    A monomial is composed of a variable ($x$), a coefficient ($a_k$) and a non-negative integer power: $a_k x^k$. A polynomial of a single variable is a function that can be expressed as a finite sum of monomial terms: $\sum_{k=1}^{n} a_k x^k$.\cite{POLYNOMIALS:1}\par
    To introduce a polynomial into the program, the user must enter the monomials in the form \verb|ax^p|, separated by a plus or minus sign. e.g. \verb|12x^3-3x^2+2|.\par
    As described in the specification, the input coefficients are integers and the powers are non-negative integers. The resulting polynomial, however, will have real coefficients.

    \begin{figure}[h!]
        \caption{Graphical user interface}
        \centering
        \includegraphics[scale=.5]{figures/gui.png}
        \label{fig:gui}
    \end{figure}

    \subsection{Use cases}
    \textbf{Use Case:} Evaluate a polynomial operation\\
    \textbf{Primary Actor:} End user\\
    \textbf{Main Success Scenario:}
    \begin{enumerate}
        \item The user selects an operation from the six possible ones: addition, subtraction,
        multiplication, division, differentiation and integration.
        \item \label{step:input1} The user enters the first polynomial.
        \item The program checks if the format of the first polynomial is valid.
        \item If the selected operation is binary:
        \begin{enumerate}
            \item \label{step:input2} The user enters the second polynomial.
            \item The program checks if the format of second the polynomial is valid.
        \end{enumerate}
        \item The user presses the `evaluate` button
        \item The program show the result of the evaluation.
    \end{enumerate}

    \textbf{Alternative Sequences:}
    \begin{enumerate}
        \item Invalid polynomial format
        \begin{itemize}
            \item The program shows the user that the format of the polynomial is incorrect by coloring the input with red.
            \item The scenario returns to step \ref{step:input1} or \ref{step:input2} depending on which input is invalid.
        \end{itemize}
        \item Denominator is zero.
        \begin{itemize}
            \item The program colors the second polynomial input with red, showing that it is invalid.
            \item The scenario returns to \ref{step:input2}.
        \end{itemize}
    \end{enumerate}


    \section{Design}
    \begin{figure}[h!]
        \caption{Package diagram}
        \centering
        \includegraphics{figures/package_diagram.1}
        \label{fig:package}
    \end{figure}

    \paragraph{}
    On figure \ref{fig:package} the relationships between the packages are shown.The view package contains the UI classes. To each panel a controller is associated. The services store the state of the application: the selected operations and the inserted polynomials. Thus the OperationsService class of service package uses the Monomial and Polynomial models from the model package. The util packages contains utility classes, for input validation, monomial and polynomial mathematical operations, and for number formatting. The services and the controllers are communicating through custom events. Custom exceptions are thrown when input format errors occur, thus the controller can catch them to alert the user.


    \section{Implementation}

    \subsection{Views}
    \label{subsec:view}
    \begin{figure}
        \caption{Views}
        \centering
        \includegraphics[width=\linewidth]{figures/view.png}
        \label{fig:view}
    \end{figure}
    The view classes use the Swing framework and the GridBagLayout\cite{Oracle:3}. These create the graphical user interface. Besides a main panel, there are four panels, each having a separate class. The AppView instantiates all these panels. \par
    To each panel a controller is associated which listens to the change in input or state, thus modifying the output, the components of the panels.\par
    OperationsSelectionPanel contains the radio buttons having as values the elements of the Operation enum. The InputPanel contains the two text fields an labels, corresponding to the two polynomial inputs. The ResultPanel contains the text field and label for the resulting polynomial, and another label-text-field pair, for the quotient after division. Finally, the ButtonPanel contains the evaluation button.

    \subsection{Models}
    \label{subsec:model}
    \begin{figure}
        \caption{model package}
        \centering
        \includegraphics[width=\linewidth]{figures/model_w_constr.png}
        \label{fig:model}
    \end{figure}

    \subsubsection{Monomial}
    The Monomial class has the purpose of representing a monomimal: a coefficient and a power. The coefficient is a double, because division and integration could produce real coefficients from integer ones.\par
    Besides a copy constructor, and a field initializer constructor, it provides a monomial constructor from a string. Using regex, the string is transformed and split by 'x' to get the values of the two attributes.

    \subsubsection{Polynomial}
    The polynomial stores the monomials in a HashMap. This provides an amortized O(1) running time for getting/setting its monomials by their power.\par
    The \verb|applyMonomial(Monomial, BinaryOperator<monomial>)| method adds a new monomial to the polynomial. The BinaryOperator has the purpose of merging the new monomial with an already existing one, having the same power. If the polynomial does not contain a monomial with the same power, `0` will be given as first parameter. Finally, if the result has the coefficient 0, the result will not be inserted, and if an element has already been inserted with the same power, it will be deleted.\par
    The addMonomial and subtractMonomial methods are using the applyMonomial by giving as parameter the add and subtract methods of the Monomials utility class.\par
    It has a copy constructor, a constructor from list of Monomials, a constructor with no parameter, and also a constructor that by parsing a string, instantiates new monomials and adds them to the existing ones.\par
    The isZero() method returns whether it contains no Monomials. The get leading term returns the Monomial which has the highest power. It's toString() method is overriden to return a string that has the same format as the input and has the simplest form.

    \subsubsection{Operation}
    The Operation enum besides containing the operation names, contains also the symbol of the operation, that will be printed, and also that whether it is a binary operator or not. The latter will determine if the second input text field should be disabled or not.

    \subsection{Services}

    \subsubsection{InputChangedService}
    \begin{figure}
        \caption{InputChangedService}
        \centering
        \includegraphics[scale=.5]{figures/input_changed_service.png}
        \label{fig:input_changed_service}
    \end{figure}
    The input changed service links the InputController and the ButtonController. The `evaluate` button is disabled unless both of the input polynomials are valid. To achieve this, a communication channel is needed between the two. Thus an InputChangedEvent is created each time the InputController receives a change in the polynomials and through this send a boolean value: whether both of the inputs are valid or not. This controller holds the listeners and activates them if the fireEvent() method is called.

    \subsubsection{OperationsService}
    \label{subsubsec:op_serv}
    \begin{figure}
        \caption{OperationsService}
        \centering
        \includegraphics[scale=.5]{figures/service.png}
        \label{fig:service}
    \end{figure}

    The OperationsService represents the state of the application. It contains the the polynomials that are read from the text fields. These are validated using the util.Validator class. It also contains the operation, which is set by the controller. \par
    Contains an EventListenerList which holds the listeners of the EvaluationEvent. The controllers are listening to this event.\par
    It's most important method is the \verb|evaluate()| method, which calls the Polynomials utility class, evaluates the two polynomials and the operation and fires a new EvaluationEvent containing the result.

    \subsection{Utility classes}
    \begin{figure}
        \caption{Utility classes}
        \centering
        \includegraphics[width=\linewidth]{figures/util.png}
        \label{fig:util}
    \end{figure}

    \subsubsection{Monomials}
    \label{subsubsec:monomials}
    The Monomials utility class provides the arithmetic operations of the monomials: addition, subtraction, multiplication, division, differentiation, integration and negation. Also provides a comparator method.

    \subsubsection{Polynomials}
    The Polynomials utility class provides the arithmetic operations of the polynomials. The \verb|divdie(Polynomial, Polynomial)| method returns a list containing two Polynomial objects, the quotient and the remainder.\cite{Wiki:2}

    \subsubsection{Validator}
    \label{subsubsec:valid}
    The Polynomials utility class provides two validation methods, one for monomials, another for polynomials. They receive a string, and using regex pattern matching, it validates the string. The regex for the monomials is built to considering a cases: only a constant is given, no power is given, +- signs are given. The regex for the polynomials specifies that the format should be composed of multiple monomials linked by a + or - sign.

    \subsubsection{Formatter}
    This utility class serves the purpose of converting a double to a string, resulting its shortest form.

    \subsection{Controllers}

    \subsubsection{InputController}
    \begin{figure}
        \caption{InputController}
        \centering
        \includegraphics[scale=.4]{figures/c_v_input.png}
        \label{fig:c_v_input}
    \end{figure}
    The InputController listens to changes in the input text fields and updates the polynomials of the OperationService with the new values. Whether or not a validation exception occured, by using the setColor method, it changes the color of the text fields to red.

    \subsubsection{OperationsSelectionController}
    \begin{figure}
        \caption{OperationsSelectionController}
        \centering
        \includegraphics[scale=.4]{figures/c_v_op.png}
        \label{fig:c_v_op}
    \end{figure}
    The OperationsSelectionController listens to the clicks on the radio buttons, and changes the operation of the OperationService according to it. Also triggers the OperationSelectionEvent, which notifies the InputController to re-validate the inputs. e.g. for divison the second polynomial must not be zero, but for the other binary operations it could be.

    \subsubsection{ButtonController}
    \begin{figure}
        \caption{ButtonController}
        \centering
        \includegraphics[scale=.4]{figures/c_v_button.png}
        \label{fig:c_v_button}
    \end{figure}
    The ButtonController listens to the InputChangedEvent and if the inputs are valid, the button becomes enabled, otherwise it will be disabled again. When the `evaluate` button is pressed the evaluate() method of the OperationService is called.

    \subsubsection{ResultController}
    \begin{figure}
        \caption{ResultController}
        \centering
        \includegraphics[scale=.4]{figures/c_v_result.png}
        \label{fig:c_v_result}
    \end{figure}
    The ResultController is listening on the operation change, so depending on the current operation it changes the label of the result field. The division operation produces two polynomials, thus for division two uneditable text fields are shown, one for the quotient, the another for the remainder.\par
    It also listens to the EvaluationEvent, so through it the controller receives the result(s) and displays it on the text fields.


    \section{Result}
    Junit5 was used to test the correct functioning of the operations and methods.The return values of the methods are verified to be equal to the given expected values.\par
    For the Monomial and Polynomial classes, the most important test case was the correct instantiation from a string.\par
    For the Monomial class, a list of strings were created, composed of all forms of valid monomials, and another list of Monomial objects having the same values as the ones in the previous list. The test case verifies that the monomials instantiated from the strings are equal to the ones in the second list. \par
    The test of the creation of Polynomial objects from strings are simmilar. Here the base cases of the monomials are stored in a list, and pairs are made from them, which are concatenated with plus and minus signs.\par
    The utility classes have all unit tests, to check that they work correctly, by applying edge case inputs to them, and verifying the output.


    \section{Conclusion}
    The main objective was achieved by completing the secondary objectives\par
    By using a HashMap to store the Monomial objects in the Polynomial, the problem of creating an object from string was further distributed to each Monomial. And the choice of the data structure provided an O(1) amortized running time for monomial search.\par
    The usage of regular expressions greatly simplified the validation of the input and the process of transforming it into an object.\par
    By using services and custom events, new connections could be created between classes that normally are inaccessible from each other. The usage of the singleton pattern guaranteed that the OperationService has only one object. Thus each class could get the same data from it. Also by listening to it's events, the controllers could react to the change in input and state.\par
    The Swing framework and the GridBagLayout showed that nice a graphical user interface can be created with not so much configuration.\par
    The unit testing proved to be an essential part of software developing, while it showed many errors and edge cases that were not initially covered.

    \paragraph{}
    The application can be further developed by:
    \begin{itemize}
        \item supporting real coefficients in the input
        \item supporting real powers in the input
        \item supporting multiple variables
        \item supporting complex numbers
    \end{itemize}

    \bibliography{bibliography}
    \bibliographystyle{plain}

\end{document}
