package com.katonaaron.calc.view;

import com.katonaaron.calc.controller.InputController;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;

public class InputPanel extends JPanel {
    private JTextField poly1 = new JTextField(10);
    private JTextField poly2 = new JTextField(10);
    private JLabel poly1Label = new JLabel("P", JLabel.CENTER);
    private JLabel poly2Label = new JLabel("Q", JLabel.CENTER);
    private Font textFieldFont = new Font("SansSerif", Font.PLAIN, 20);

    private InputController controller = new InputController(this);
    private GridBagConstraints constraints = new GridBagConstraints();

    public InputPanel() {
        super(new GridBagLayout());

        this.setBorder(new TitledBorder("Enter the polynomial"));
        addInputFields();
    }

    private void addInputFields() {
        constraints.anchor = GridBagConstraints.WEST;

        poly1Label.setLabelFor(poly1);
        constraints.gridx = 0;
        constraints.gridy = 0;
        this.add(poly1Label, constraints);

        poly2Label.setLabelFor(poly2);
        constraints.gridx = 0;
        constraints.gridy = 1;
        this.add(poly2Label, constraints);

        constraints.anchor = GridBagConstraints.EAST;
        constraints.weightx = 1.0;

        poly1.setFont(textFieldFont);
        poly1.getDocument().addDocumentListener(controller);
        constraints.gridx = 1;
        constraints.gridy = 0;
        this.add(poly1, constraints);

        poly2.setFont(textFieldFont);
        poly2.getDocument().addDocumentListener(controller);
        constraints.gridx = 1;
        constraints.gridy = 1;
        this.add(poly2, constraints);
    }

    public JTextField getPolynomialInput1() {
        return poly1;
    }

    public JTextField getPolynomialInput2() {
        return poly2;
    }
}
