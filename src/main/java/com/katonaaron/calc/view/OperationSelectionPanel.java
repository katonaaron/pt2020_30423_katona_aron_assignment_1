package com.katonaaron.calc.view;

import com.katonaaron.calc.controller.OperationSelectionController;
import com.katonaaron.calc.model.Operation;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;

public class OperationSelectionPanel extends JPanel {
    private OperationSelectionController controller = new OperationSelectionController();
    private GridBagConstraints constraints = new GridBagConstraints(
            0,
            0,
            GridBagConstraints.RELATIVE,
            1,
            1.0,
            1.0,
            GridBagConstraints.CENTER,
            GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0),
            0,
            0
    );

    public OperationSelectionPanel() {
        super(new GridBagLayout());

        this.setBorder(new TitledBorder("Select the operation"));

        addRadioButtons();
    }

    private void addRadioButtons() {
        ButtonGroup group = new ButtonGroup();

        for (Operation op : Operation.values()) {
            JRadioButton button = new JRadioButton(op.toString().toLowerCase());
            button.setActionCommand(op.toString());
            button.addActionListener(controller);

            group.add(button);
            this.add(button, constraints);
            constraints.gridy++;
        }

        group.getElements().nextElement().doClick();
    }
}
