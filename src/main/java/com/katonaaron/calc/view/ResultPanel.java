package com.katonaaron.calc.view;

import com.katonaaron.calc.controller.ResultController;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;

public class ResultPanel extends JPanel {
    private JTextField result = new JTextField(10);
    private JTextField remainder = new JTextField(10);
    private JLabel resultLabel = new JLabel("P + Q", JLabel.CENTER);
    private JLabel remainderLabel = new JLabel("P%Q", JLabel.CENTER);
    private Font textFieldFont = new Font("SansSerif", Font.PLAIN, 20);

    private ResultController controller = new ResultController(this);
    private GridBagConstraints constraints = new GridBagConstraints();

    public ResultPanel() {
        super(new GridBagLayout());

        this.setBorder(new TitledBorder("Result"));

        addResultField();
    }

    private void addResultField() {
        constraints.anchor = GridBagConstraints.WEST;

        resultLabel.setLabelFor(result);
        constraints.gridx = 0;
        constraints.gridy = 0;
        this.add(resultLabel, constraints);

        remainderLabel.setLabelFor(remainder);
        constraints.gridx = 0;
        constraints.gridy = 1;
        this.add(remainderLabel, constraints);

        constraints.anchor = GridBagConstraints.EAST;
        constraints.weightx = 1.0;

        result.setFont(textFieldFont);
        result.setEditable(false);
        constraints.gridx = 1;
        constraints.gridy = 0;
        this.add(result, constraints);

        remainder.setFont(textFieldFont);
        remainder.setEditable(false);
        constraints.gridx = 1;
        constraints.gridy = 1;
        this.add(remainder, constraints);
    }

    public JLabel getResultLabel() {
        return resultLabel;
    }

    public JTextField getResult() {
        return result;
    }

    public JTextField getRemainder() {
        return remainder;
    }

    public JLabel getRemainderLabel() {
        return remainderLabel;
    }
}
