package com.katonaaron.calc.view;

import javax.swing.*;
import java.awt.*;

public class AppView extends JFrame {
    private JPanel mainPanel = new JPanel(new GridBagLayout());
    private InputPanel inputPanel = new InputPanel();
    private ResultPanel resultPanel = new ResultPanel();
    private ButtonPanel buttonPanel = new ButtonPanel();
    private OperationSelectionPanel opPanel = new OperationSelectionPanel();

    private GridBagConstraints constraints = new GridBagConstraints(
            0,
            0,
            1,
            1,
            1.0,
            1.0,
            GridBagConstraints.CENTER,
            GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0),
            30,
            30
    );

    public AppView(String title) {
        super(title);

        addPanels();
        getRootPane().setDefaultButton(buttonPanel.getEvaluateButton());

        this.add(mainPanel);
    }

    private void addPanels() {
        constraints.gridx = constraints.gridy = 0;
        mainPanel.add(inputPanel, constraints);

        constraints.gridx = 0;
        constraints.gridy = 1;
        constraints.ipadx = 100;
        mainPanel.add(resultPanel, constraints);

        constraints.gridx = 1;
        constraints.gridy = 0;
        constraints.gridwidth = 1;
        constraints.gridheight = 2;
        constraints.ipadx = 60;
        mainPanel.add(opPanel, constraints);

        constraints.gridx = 0;
        constraints.gridy = 2;
        constraints.gridwidth = 2;
        constraints.gridheight = 1;
        constraints.ipadx = 30;
        mainPanel.add(buttonPanel, constraints);
    }
}
