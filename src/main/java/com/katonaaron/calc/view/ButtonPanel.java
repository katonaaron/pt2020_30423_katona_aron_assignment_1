package com.katonaaron.calc.view;

import com.katonaaron.calc.controller.ButtonController;

import javax.swing.*;
import java.awt.*;

public class ButtonPanel extends JPanel {
    private JButton evaluateButton = new JButton("evaluate");

    private ButtonController controller = new ButtonController(this);

    public ButtonPanel() {
        super(new GridBagLayout());
        addButtons();
    }

    void addButtons() {
        evaluateButton.addActionListener(controller);
        evaluateButton.setEnabled(false);
        this.add(evaluateButton, new GridBagConstraints());
    }

    public JButton getEvaluateButton() {
        return evaluateButton;
    }
}
