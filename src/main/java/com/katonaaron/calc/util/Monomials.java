package com.katonaaron.calc.util;

import com.katonaaron.calc.exception.DivisionByZeroMonomialException;
import com.katonaaron.calc.exception.MonomialPowerMismatchException;
import com.katonaaron.calc.model.Monomial;

public final class Monomials {
    public static int compare(Monomial x, Monomial y) {
        return x.getPower() < y.getPower() ? -1 :
                x.getPower() == y.getPower() ? Double.compare(x.getCoefficient(), y.getCoefficient()) : 1;
    }

    public static Monomial add(Monomial a, Monomial b) {
        if (a.getPower() != b.getPower()) {
            throw new MonomialPowerMismatchException("Monomials with same powers can be added only");
        }
        return new Monomial(a.getCoefficient() + b.getCoefficient(), a.getPower());
    }

    public static Monomial subtract(Monomial a, Monomial b) {
        if (a.getPower() != b.getPower()) {
            throw new MonomialPowerMismatchException("Monomials with same powers can be subtracted only");
        }
        return new Monomial(a.getCoefficient() - b.getCoefficient(), a.getPower());
    }

    public static Monomial multiply(Monomial a, Monomial b) {
        return new Monomial(a.getCoefficient() * b.getCoefficient(), a.getPower() + b.getPower());
    }

    public static Monomial divide(Monomial a, Monomial b) {
        if (b.getCoefficient() == 0) {
            throw new DivisionByZeroMonomialException("Divisor is 0");
        }
        return new Monomial(a.getCoefficient() / b.getCoefficient(), a.getPower() - b.getPower());
    }

    public static Monomial differentiate(Monomial m) {
        final int power = m.getPower();

        if (power == 0) {
            return new Monomial(0, 0);
        }
        return new Monomial(m.getCoefficient() * power, power - 1);
    }

    public static Monomial integrate(Monomial m) {
        final int power = m.getPower() + 1;
        return new Monomial(m.getCoefficient() / power, power);
    }

    public static Monomial negate(Monomial m) {
        return new Monomial(-m.getCoefficient(), m.getPower());
    }
}
