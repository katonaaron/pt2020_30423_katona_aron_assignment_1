package com.katonaaron.calc.util;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

public final class Formatter {
    private static DecimalFormat df;

    static {
        df = new DecimalFormat("0", DecimalFormatSymbols.getInstance(Locale.ENGLISH));
        df.setMaximumFractionDigits(340);
    }

    public static String doubleToString(double number) {
        return df.format(number);
    }
}
