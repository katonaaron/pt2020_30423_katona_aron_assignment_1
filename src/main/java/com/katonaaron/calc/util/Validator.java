package com.katonaaron.calc.util;

import com.katonaaron.calc.exception.InvalidMonomialFormatException;
import com.katonaaron.calc.exception.InvalidPolynomialFormatException;

public final class Validator {
    private static final String monomialRegex = "(([-+]?[0-9]+)|([-+]?[0-9]*x(\\^([+]?[0-9]+))?))";
    private static final String completeMonomialRegex = "^" + monomialRegex + "$";
    private static final String polynomialRegex = "^" + monomialRegex + "([-+]" + monomialRegex + ")*$";

    public static void validatePolynomial(String polynomial) throws InvalidPolynomialFormatException {
        if (!polynomial.matches(polynomialRegex)) {
            throw new InvalidPolynomialFormatException(polynomial + "is not a valid polynomial");
        }
    }

    public static void validateMonomial(String monomial) throws InvalidMonomialFormatException {
        if (!monomial.matches(completeMonomialRegex)) {
            throw new InvalidMonomialFormatException(monomial + "is not a valid monomial");
        }
    }
}
