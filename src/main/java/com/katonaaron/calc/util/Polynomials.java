package com.katonaaron.calc.util;

import com.katonaaron.calc.exception.DivisionByZeroPolynomialException;
import com.katonaaron.calc.model.Monomial;
import com.katonaaron.calc.model.Polynomial;

import java.util.List;
import java.util.stream.Collectors;

public final class Polynomials {
    public static Polynomial add(Polynomial a, Polynomial b) {
        Polynomial p = new Polynomial(a);

        b.getMonomials().forEach(p::addMonomial);

        return p;
    }

    public static Polynomial subtract(Polynomial a, Polynomial b) {
        Polynomial p = new Polynomial(a);

        b.getMonomials().forEach(p::subtractMonomial);

        return p;
    }

    public static Polynomial multiply(Polynomial a, Polynomial b) {
        Polynomial p = new Polynomial();

        List<Monomial> monomialList = b.getMonomials();

        for (Monomial m1 : a.getMonomials()) {
            for (Monomial m2 : monomialList) {
                p.addMonomial(Monomials.multiply(m1, m2));
            }
        }

        return p;
    }

    public static List<Polynomial> divide(Polynomial n, Polynomial d) {
        if (d.isZero()) {
            throw new DivisionByZeroPolynomialException();
        }

        Polynomial q = new Polynomial();
        Polynomial r = new Polynomial(n);

        Monomial leadR = r.getLeadingTerm();
        Monomial leadD = d.getLeadingTerm();
        Monomial t;

        while (!r.isZero() && leadR.getPower() >= leadD.getPower()) {
            t = Monomials.divide(leadR, leadD);
            q.setMonomial(t);
            r = subtract(r, multiply(new Polynomial(List.of(t)), d));

            leadR = r.getLeadingTerm();
            leadD = d.getLeadingTerm();
        }

        return List.of(q, r);
    }

    public static Polynomial differentiate(Polynomial p) {
        return new Polynomial(
                p.getMonomials().stream()
                        .map(Monomials::differentiate)
                        .collect(Collectors.toList())
        );
    }


    public static Polynomial integrate(Polynomial p) {
        return new Polynomial(
                p.getMonomials().stream()
                        .map(Monomials::integrate)
                        .collect(Collectors.toList())
        );
    }
}
