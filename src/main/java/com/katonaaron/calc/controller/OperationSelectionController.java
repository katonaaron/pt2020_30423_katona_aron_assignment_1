package com.katonaaron.calc.controller;

import com.katonaaron.calc.model.Operation;
import com.katonaaron.calc.service.OperationService;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class OperationSelectionController implements ActionListener {

    private OperationService operationService = OperationService.getInstance();

    @Override
    public void actionPerformed(ActionEvent e) {
        JRadioButton radioButton = (JRadioButton) e.getSource();
        operationService.setOperation(Operation.valueOf(radioButton.getActionCommand()));
    }
}
