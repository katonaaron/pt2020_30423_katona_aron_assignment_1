package com.katonaaron.calc.controller;

import com.katonaaron.calc.event.OperationSelectionEvent;
import com.katonaaron.calc.event.OperationSelectionListener;
import com.katonaaron.calc.exception.DivisionByZeroPolynomialException;
import com.katonaaron.calc.exception.InvalidPolynomialFormatException;
import com.katonaaron.calc.model.Operation;
import com.katonaaron.calc.service.InputChangedService;
import com.katonaaron.calc.service.OperationService;
import com.katonaaron.calc.view.InputPanel;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;

public class InputController implements OperationSelectionListener, DocumentListener {
    private InputPanel view;
    private OperationService operationService = OperationService.getInstance();
    private InputChangedService inputChangedService = InputChangedService.getInstance();

    public InputController(InputPanel view) {
        this.view = view;
        operationService.addOperationSelectionListener(this);
    }

    @Override
    public void selectionPerformed(OperationSelectionEvent event) {
        Operation operation = (Operation) event.getSource();
        view.getPolynomialInput2().setEditable(operation.isBinary());
        updatePolynomials();
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        updatePolynomials();
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        updatePolynomials();
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
        updatePolynomials();
    }

    private void updatePolynomials() {
        boolean isValid = updatePolynomial(1);
        inputChangedService.fireEvent(updatePolynomial(2) && isValid);
    }

    private boolean updatePolynomial(int nr) {
        JTextField textField = nr == 2 ? view.getPolynomialInput2() : view.getPolynomialInput1();

        try {
            if (nr == 2) {
                operationService.setPolynomial2(textField.getText());
                if (operationService.getOperation() == Operation.DIVIDE && operationService.getPolynomial2().isZero()) {
                    throw new DivisionByZeroPolynomialException();
                }
            } else {
                operationService.setPolynomial1(textField.getText());
            }
            setColor(textField, true);
        } catch (InvalidPolynomialFormatException | DivisionByZeroPolynomialException e) {
            setColor(textField, false);
            return false;
        }
        return true;
    }

    private void setColor(JTextField textField, boolean isCorrect) {
        if (!isCorrect && textField.isEditable()) {
            textField.setForeground(Color.RED);
        } else {
            textField.setForeground(Color.BLACK);
        }
    }
}
