package com.katonaaron.calc.controller;

import com.katonaaron.calc.event.InputChangedEvent;
import com.katonaaron.calc.event.InputChangedListener;
import com.katonaaron.calc.service.InputChangedService;
import com.katonaaron.calc.service.OperationService;
import com.katonaaron.calc.view.ButtonPanel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ButtonController implements ActionListener, InputChangedListener {

    private ButtonPanel view;
    private OperationService operationService = OperationService.getInstance();
    private InputChangedService inputChangedService = InputChangedService.getInstance();

    public ButtonController(ButtonPanel view) {
        this.view = view;
        inputChangedService.addListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == view.getEvaluateButton()) {
            operationService.evaluate();
        }
    }

    @Override
    public void inputChanged(InputChangedEvent event) {
        view.getEvaluateButton().setEnabled((Boolean) event.getSource());
    }
}
