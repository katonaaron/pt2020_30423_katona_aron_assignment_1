package com.katonaaron.calc.controller;

import com.katonaaron.calc.event.EvaluationEvent;
import com.katonaaron.calc.event.EvaluationListener;
import com.katonaaron.calc.event.OperationSelectionEvent;
import com.katonaaron.calc.event.OperationSelectionListener;
import com.katonaaron.calc.model.Operation;
import com.katonaaron.calc.model.Polynomial;
import com.katonaaron.calc.service.OperationService;
import com.katonaaron.calc.view.ResultPanel;

import java.util.List;

public class ResultController implements OperationSelectionListener, EvaluationListener {

    private ResultPanel view;
    private OperationService operationService = OperationService.getInstance();

    public ResultController(ResultPanel view) {
        this.view = view;
        operationService.addOperationSelectionListener(this);
        operationService.addEvaluationListener(this);
    }

    @Override
    public void selectionPerformed(OperationSelectionEvent event) {
        Operation operation = (Operation) event.getSource();
        view.getResultLabel().setText(operation.getSymbol());
        view.getRemainder().setVisible(operation == Operation.DIVIDE);
        view.getRemainderLabel().setVisible(operation == Operation.DIVIDE);

        view.getResult().setText("");
        view.getRemainder().setText("");
    }

    @Override
    public void evaluationPerformed(EvaluationEvent event) {
        if (event.getSource() instanceof List) {
            List<Polynomial> polynomials = (List<Polynomial>) event.getSource();
            view.getResult().setText(polynomials.get(0).toString());
            view.getRemainder().setText(polynomials.get(1).toString());
        } else {
            Polynomial p = (Polynomial) event.getSource();
            view.getResult().setText(p == null ? "" : p.toString());
        }
    }
}
