package com.katonaaron.calc;

import com.katonaaron.calc.view.AppView;

import javax.swing.*;
import java.net.URL;

public class App {
    public static void main(String[] args) {
        JFrame frame = new AppView("Polynomial Calculator");
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.pack();
        frame.setLocationRelativeTo(null);

        URL iconURL = App.class.getResource("/icon.png");
        ImageIcon icon = new ImageIcon(iconURL);

        frame.setIconImage(icon.getImage());
        frame.setVisible(true);
    }
}
