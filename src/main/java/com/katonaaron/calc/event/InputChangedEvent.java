package com.katonaaron.calc.event;

import java.util.EventObject;

public class InputChangedEvent extends EventObject {

    public InputChangedEvent(boolean isValid) {
        super(isValid);
    }
}
