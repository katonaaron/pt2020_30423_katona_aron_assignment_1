package com.katonaaron.calc.event;

import java.util.EventObject;

public class OperationSelectionEvent extends EventObject {

    /**
     * Constructs a prototypical Event.
     *
     * @param source the object on which the Event initially occurred
     * @throws IllegalArgumentException if source is null
     */
    public OperationSelectionEvent(Object source) {
        super(source);
    }
}
