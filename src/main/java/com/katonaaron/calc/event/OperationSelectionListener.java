package com.katonaaron.calc.event;

import java.util.EventListener;

public interface OperationSelectionListener extends EventListener {
    void selectionPerformed(OperationSelectionEvent event);
}
