package com.katonaaron.calc.event;

import java.util.EventListener;

public interface EvaluationListener extends EventListener {
    void evaluationPerformed(EvaluationEvent event);
}
