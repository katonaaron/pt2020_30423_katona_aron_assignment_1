package com.katonaaron.calc.event;

import java.util.EventListener;

public interface InputChangedListener extends EventListener {
    void inputChanged(InputChangedEvent event);
}
