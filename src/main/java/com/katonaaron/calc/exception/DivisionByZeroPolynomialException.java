package com.katonaaron.calc.exception;

public class DivisionByZeroPolynomialException extends IllegalArgumentException {
    public DivisionByZeroPolynomialException() {
    }

    public DivisionByZeroPolynomialException(String message) {
        super(message);
    }

    public DivisionByZeroPolynomialException(String message, Throwable cause) {
        super(message, cause);
    }

    public DivisionByZeroPolynomialException(Throwable cause) {
        super(cause);
    }
}
