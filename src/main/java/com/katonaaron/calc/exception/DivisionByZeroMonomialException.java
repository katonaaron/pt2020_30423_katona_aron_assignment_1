package com.katonaaron.calc.exception;

public class DivisionByZeroMonomialException extends IllegalArgumentException {
    public DivisionByZeroMonomialException() {
    }

    public DivisionByZeroMonomialException(String message) {
        super(message);
    }

    public DivisionByZeroMonomialException(String message, Throwable cause) {
        super(message, cause);
    }

    public DivisionByZeroMonomialException(Throwable cause) {
        super(cause);
    }
}
