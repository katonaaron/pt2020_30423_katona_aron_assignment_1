package com.katonaaron.calc.exception;

public class MonomialPowerMismatchException extends RuntimeException {
    public MonomialPowerMismatchException() {
    }

    public MonomialPowerMismatchException(String message) {
        super(message);
    }

    public MonomialPowerMismatchException(String message, Throwable cause) {
        super(message, cause);
    }

    public MonomialPowerMismatchException(Throwable cause) {
        super(cause);
    }
}
