package com.katonaaron.calc.exception;

public class InvalidPolynomialFormatException extends Exception {
    public InvalidPolynomialFormatException() {
    }

    public InvalidPolynomialFormatException(String message) {
        super(message);
    }

    public InvalidPolynomialFormatException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidPolynomialFormatException(Throwable cause) {
        super(cause);
    }
}
