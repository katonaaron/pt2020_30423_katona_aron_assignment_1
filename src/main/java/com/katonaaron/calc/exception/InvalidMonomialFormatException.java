package com.katonaaron.calc.exception;

public class InvalidMonomialFormatException extends Exception {
    public InvalidMonomialFormatException() {
    }

    public InvalidMonomialFormatException(String message) {
        super(message);
    }

    public InvalidMonomialFormatException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidMonomialFormatException(Throwable cause) {
        super(cause);
    }
}
