package com.katonaaron.calc.model;

import com.katonaaron.calc.util.Monomials;

import java.util.*;
import java.util.function.BinaryOperator;

public class Polynomial {
    private Map<Integer, Monomial> monomials = new HashMap<>();

    public Polynomial() {
    }

    public Polynomial(Polynomial original) {
        monomials = new HashMap<>(original.monomials);
    }

    public Polynomial(List<Monomial> monomials) {
        monomials.forEach(this::addMonomial);
    }

    public Polynomial(String polynomial) {
        if ("".equals(polynomial)) {
            return;
        }

        String[] monomialStrings = polynomial
                .replaceAll(" +", "")
                .replaceAll("^\\+", "")
                .replaceAll("(?!^)-", "+-")
                .replaceAll("\\Q^+\\E", "^")
                .split("\\Q+\\E");

        Arrays.stream(monomialStrings)
                .map(Monomial::new)
                .forEach(this::addMonomial);
    }

    public boolean isZero() {
        return monomials.size() == 0;
    }

    public Monomial getLeadingTerm() {
        if (isZero()) {
            return null;
        }
        return monomials.get(Collections.max(monomials.keySet()));
    }

    public Monomial getMonomial(int power) {
        return monomials.get(power);
    }

    public void setMonomial(Monomial m) {
        monomials.put(m.getPower(), new Monomial(m));
    }

    public void removeMonomial(Monomial m) {
        monomials.remove(m.getPower());
    }

    public void addMonomial(Monomial m) {
        applyMonomial(m, Monomials::add);
    }

    public void subtractMonomial(Monomial m) {
        applyMonomial(m, Monomials::subtract);
    }

    public void applyMonomial(Monomial m, BinaryOperator<Monomial> operator) {
        Monomial currentMonomial = monomials.get(m.getPower());

        if (currentMonomial == null) {
            currentMonomial = new Monomial(0, m.getPower());
        }

        Monomial monomial = operator.apply(currentMonomial, m);

        if (monomial.isZero()) {
            removeMonomial(monomial);
        } else {
            setMonomial(monomial);
        }
    }

    public List<Monomial> getMonomials() {
        return new ArrayList<>(monomials.values());
    }

    @Override
    public String toString() {
        if (isZero()) {
            return "0";
        }
        return monomials.values().stream()
                .sorted((a, b) -> Monomials.compare(b, a))
                .map(Monomial::toString)
                .reduce((m1, m2) -> m1 + "+" + m2)
                .orElse("")
                .replaceAll("\\Q+-\\E", "-");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Polynomial)) return false;
        Polynomial that = (Polynomial) o;
        return Objects.equals(monomials, that.monomials);
    }

    @Override
    public int hashCode() {
        return Objects.hash(monomials);
    }
}
