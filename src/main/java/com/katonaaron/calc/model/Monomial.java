package com.katonaaron.calc.model;

import com.katonaaron.calc.util.Formatter;

import java.util.Objects;

public class Monomial {
    private double coefficient;
    private int power;

    public Monomial(double coefficient, int power) {
        setCoefficient(coefficient);
        setPower(power);
    }

    public Monomial(Monomial m) {
        this(m.coefficient, m.power);
    }

    public Monomial(String monomial) {
        monomial = monomial
                .replaceAll(" +", "")
                .toLowerCase()
                .replaceAll("-0", "0");

        if (!monomial.contains("x")) {
            coefficient = Double.parseDouble(monomial);
            power = 0;
            return;
        }

        String[] parts = monomial
                .replaceFirst("-x", "-1x")
                .replaceFirst("^x", "1x")
                .replaceFirst("x$", "x^1")
                .split("x");

        coefficient = Double.parseDouble(parts[0]);

        if (coefficient == 0) {
            power = 0;
        } else {
            power = Integer.parseInt(parts[1].substring(1));
        }
    }

    public boolean isZero() {
        return coefficient == 0;
    }

    public double getCoefficient() {
        return coefficient;
    }

    public void setCoefficient(double coefficient) {
        this.coefficient = coefficient;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        if (power < 0) {
            throw new IllegalArgumentException("power must be >= 0");
        }
        this.power = power;
    }

    @Override
    public String toString() {
        if (coefficient == 0)
            return "0";

        if (power == 0) {
            return Formatter.doubleToString(coefficient);
        }

        String string;
        if (coefficient == 1) {
            string = "x";
        } else if (coefficient == -1) {
            string = "-x";
        } else {
            string = Formatter.doubleToString(coefficient) + "x";
        }
        return power == 1 ? string : string + "^" + power;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Monomial)) return false;
        Monomial monomial = (Monomial) o;
        return Double.compare(monomial.getCoefficient(), getCoefficient()) == 0 &&
                Double.compare(monomial.getPower(), getPower()) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCoefficient(), getPower());
    }
}
