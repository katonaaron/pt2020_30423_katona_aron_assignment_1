package com.katonaaron.calc.model;

public enum Operation {
    ADD("P + Q", true),
    SUBTRACT("P - Q", true),
    MULTIPLY("P * Q", true),
    DIVIDE("P / Q", true),
    DIFFERENTIATE("P'", false),
    INTEGRATE("∫Pdx", false);

    private String symbol;
    private boolean binary;

    Operation(String symbol, boolean binary) {
        this.symbol = symbol;
        this.binary = binary;
    }

    public String getSymbol() {
        return symbol;
    }

    public boolean isBinary() {
        return binary;
    }
}
