package com.katonaaron.calc.service;

import com.katonaaron.calc.event.EvaluationEvent;
import com.katonaaron.calc.event.EvaluationListener;
import com.katonaaron.calc.event.OperationSelectionEvent;
import com.katonaaron.calc.event.OperationSelectionListener;
import com.katonaaron.calc.exception.InvalidPolynomialFormatException;
import com.katonaaron.calc.model.Operation;
import com.katonaaron.calc.model.Polynomial;
import com.katonaaron.calc.util.Polynomials;
import com.katonaaron.calc.util.Validator;

import javax.swing.event.EventListenerList;
import java.util.Arrays;

public class OperationService {
    private static OperationService instance;

    private EventListenerList listenerList = new EventListenerList();

    private Operation operation;
    private Polynomial polynomial1;
    private Polynomial polynomial2;

    private OperationService() {
    }

    public static OperationService getInstance() {
        if (instance == null) {
            instance = new OperationService();
        }
        return instance;
    }

    public Operation getOperation() {
        return operation;
    }

    public void setOperation(Operation operation) {
        this.operation = operation;
        fireOperationSelectionEvent(new OperationSelectionEvent(operation));
    }

    public Polynomial getPolynomial1() {
        return polynomial1;
    }

    public void setPolynomial1(String polynomial) throws InvalidPolynomialFormatException {
        polynomial = polynomial.replaceAll(" +", "");
        Validator.validatePolynomial(polynomial);
        this.polynomial1 = new Polynomial(polynomial);
    }

    public Polynomial getPolynomial2() {
        return polynomial2;
    }

    public void setPolynomial2(String polynomial) throws InvalidPolynomialFormatException {
        polynomial = polynomial.replaceAll(" +", "");
        Validator.validatePolynomial(polynomial);
        this.polynomial2 = new Polynomial(polynomial);
    }

    public void evaluate() {
        Object result;

        switch (operation) {
            case ADD:
                result = Polynomials.add(polynomial1, polynomial2);
                break;
            case SUBTRACT:
                result = Polynomials.subtract(polynomial1, polynomial2);
                break;
            case MULTIPLY:
                result = Polynomials.multiply(polynomial1, polynomial2);
                break;
            case DIVIDE:
                result = Polynomials.divide(polynomial1, polynomial2);
                break;
            case DIFFERENTIATE:
                result = Polynomials.differentiate(polynomial1);
                break;
            case INTEGRATE:
                result = Polynomials.integrate(polynomial1);
                break;
            default:
                throw new UnsupportedOperationException();
        }

        fireEvaluationEvent(new EvaluationEvent(result));
    }

    public void addOperationSelectionListener(OperationSelectionListener listener) {
        listenerList.add(OperationSelectionListener.class, listener);
    }

    public void removeOperationSelectionListener(OperationSelectionListener listener) {
        listenerList.remove(OperationSelectionListener.class, listener);
    }

    private void fireOperationSelectionEvent(OperationSelectionEvent event) {
        Arrays.stream(listenerList.getListeners(OperationSelectionListener.class))
                .forEach(listener -> listener.selectionPerformed(event));
    }

    public void addEvaluationListener(EvaluationListener listener) {
        listenerList.add(EvaluationListener.class, listener);
    }

    public void removeEvaluationListener(EvaluationListener listener) {
        listenerList.remove(EvaluationListener.class, listener);
    }

    private void fireEvaluationEvent(EvaluationEvent event) {
        Arrays.stream(listenerList.getListeners(EvaluationListener.class))
                .forEach(listener -> listener.evaluationPerformed(event));
    }
}
