package com.katonaaron.calc.service;

import com.katonaaron.calc.event.InputChangedEvent;
import com.katonaaron.calc.event.InputChangedListener;

import javax.swing.event.EventListenerList;
import java.util.Arrays;

public class InputChangedService {

    private static InputChangedService instance;
    private EventListenerList listenerList = new EventListenerList();

    private InputChangedService() {

    }

    public static InputChangedService getInstance() {
        if (instance == null) {
            instance = new InputChangedService();
        }
        return instance;
    }

    public void addListener(InputChangedListener listener) {
        listenerList.add(InputChangedListener.class, listener);
    }

    public void removeListener(InputChangedListener listener) {
        listenerList.remove(InputChangedListener.class, listener);
    }

    public void fireEvent(boolean isValid) {
        Arrays.stream(listenerList.getListeners(InputChangedListener.class))
                .forEach(listener -> listener.inputChanged(new InputChangedEvent(isValid)));
    }
}
