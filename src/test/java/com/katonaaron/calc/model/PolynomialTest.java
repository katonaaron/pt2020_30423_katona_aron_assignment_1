package com.katonaaron.calc.model;

import com.katonaaron.calc.util.Monomials;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.BinaryOperator;

class PolynomialTest {
    private static final List<String> monomialStrings = List.of(
            "x", "1x", "x^1", "1x^1", "25x", "x^42", "12x^31",
            "12", "12x^0", "0", "0x^0", "x^0", "1x^0"
    );

    private static final List<Monomial> monomials = List.of(
            new Monomial(1.0, 1),
            new Monomial(1.0, 1),
            new Monomial(1.0, 1),
            new Monomial(1.0, 1),
            new Monomial(25.0, 1),
            new Monomial(1.0, 42),
            new Monomial(12.0, 31),
            new Monomial(12.0, 0),
            new Monomial(12.0, 0),
            new Monomial(0.0, 0),
            new Monomial(0.0, 0),
            new Monomial(1.0, 0),
            new Monomial(1.0, 0)
    );
    private static final List<BinaryOperator<String>> stringOperators = List.of(
            (a, b) -> a + "+" + b,
            (a, b) -> a + "-" + b,
            (a, b) -> "-" + a + "+" + b,
            (a, b) -> "+" + a + "+" + b,
            (a, b) -> "-" + a + "-" + b,
            (a, b) -> "+" + a + "-" + b
    );
    private static final Monomial zero = new Monomial(0, 0);
    private static final List<BiFunction<Monomial, Monomial, Polynomial>> monomialOperators = List.of(
            (a, b) -> new Polynomial(List.of(a, b)),
            (a, b) -> new Polynomial(List.of(a, Monomials.negate(b))),
            (a, b) -> new Polynomial(List.of(Monomials.negate(a), b)),
            (a, b) -> new Polynomial(List.of(a, b)),
            (a, b) -> new Polynomial(List.of(Monomials.negate(a), Monomials.negate(b))),
            (a, b) -> new Polynomial(List.of(a, Monomials.negate(b)))
    );

    public static List<String> polynomialStrings = new ArrayList<>();
    public static List<Polynomial> polynomials = new ArrayList<>();

    @BeforeAll
    public static void init() {
        for (String m1 : monomialStrings) {
            for (String m2 : monomialStrings) {
                for (BinaryOperator<String> op : stringOperators) {
                    polynomialStrings.add(op.apply(m1, m2));
                }
            }
        }
        for (Monomial m1 : monomials) {
            for (Monomial m2 : monomials) {
                for (BiFunction<Monomial, Monomial, Polynomial> op : monomialOperators) {
                    polynomials.add(op.apply(m1, m2));
                }
            }
        }
    }


    @Test
    void testCreationFromString() {
        for (int i = 0; i < polynomials.size(); i++) {
            Assertions.assertEquals(polynomials.get(i), new Polynomial(polynomialStrings.get(i)));
        }
    }

    @Test
    void isZero() {
        Assertions.assertTrue(new Polynomial("").isZero());
        Assertions.assertTrue(new Polynomial("0").isZero());
        Assertions.assertTrue(new Polynomial("0x").isZero());
        Assertions.assertTrue(new Polynomial("0x^2").isZero());
        Assertions.assertTrue(new Polynomial("0x^0").isZero());
        Assertions.assertTrue(new Polynomial("-0").isZero());
        Assertions.assertTrue(new Polynomial("-0x").isZero());
        Assertions.assertTrue(new Polynomial("-0x^2").isZero());
        Assertions.assertTrue(new Polynomial("-0x^0").isZero());
        Assertions.assertTrue(new Polynomial("+0").isZero());
        Assertions.assertTrue(new Polynomial("+0x").isZero());
        Assertions.assertTrue(new Polynomial("+0x^2").isZero());
        Assertions.assertTrue(new Polynomial("+0x^0").isZero());
        Assertions.assertFalse(new Polynomial("x").isZero());
        Assertions.assertFalse(new Polynomial("x^0").isZero());
        Assertions.assertFalse(new Polynomial("x^0").isZero());
    }

    @Test
    void getLeadingTerm() {
        Assertions.assertNull(new Polynomial("").getLeadingTerm());
        Assertions.assertEquals(new Monomial("1"), new Polynomial("1").getLeadingTerm());
        Assertions.assertEquals(new Monomial("2x^2"), new Polynomial("2x^2 + 3x + 2").getLeadingTerm());
    }

    @Test
    void testToString() {
        Assertions.assertEquals("0", new Polynomial("").toString());
        Assertions.assertEquals("0", new Polynomial("0x").toString());
    }
}
