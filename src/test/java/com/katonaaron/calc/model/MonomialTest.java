package com.katonaaron.calc.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

class MonomialTest {
    public static final List<String> monomials = List.of(
            "x", "-x", "1x", "-1x",
            "x^1", "-x^1", /*"x^-1", "-x^-1",*/
            "1x^1", "-1x^1", /*"1x^-1", "-1x^-1",*/
            "25x", "-32x",
            "x^42", "-x^13", /*"x^-91", "-x^-31",*/
            "12x^31", "-65x^32", /*"65x^-76", "-435x^-234",*/
            "12", "12x^0", "0", "0x^0", "x^0", "-2x^-0", "-0", "-0x", "-0x^2", "1x^0",
            "+5", "+2x^+2"
    );
    public static final List<Monomial> expectedMonomial = List.of(
            new Monomial(1.0, 1),
            new Monomial(-1.0, 1),
            new Monomial(1.0, 1),
            new Monomial(-1.0, 1),
            new Monomial(1.0, 1),
            new Monomial(-1.0, 1),
//            new Monomial(1.0, -1),
//            new Monomial(-1.0, -1),
            new Monomial(1.0, 1),
            new Monomial(-1.0, 1),
//            new Monomial(1.0, -1),
//            new Monomial(-1.0, -1),
            new Monomial(25.0, 1),
            new Monomial(-32.0, 1),
            new Monomial(1.0, 42),
            new Monomial(-1.0, 13),
//            new Monomial(1.0, -91),
//            new Monomial(-1.0, -31),
            new Monomial(12.0, 31),
            new Monomial(-65.0, 32),
//            new Monomial(65.0, -76),
//            new Monomial(-435.0, -234),
            new Monomial(12.0, 0),
            new Monomial(12.0, 0),
            new Monomial(0.0, 0),
            new Monomial(0.0, 0),
            new Monomial(1.0, 0),
            new Monomial(-2.0, 0),
            new Monomial(0.0, 0),
            new Monomial(0.0, 0),
            new Monomial(0.0, 0),
            new Monomial(1.0, 0),
            new Monomial(5.0, 0),
            new Monomial(2.0, 2)
    );
    private static final List<String> expectedString = List.of(
            "x", "-x", "x", "-x",
            "x", "-x", /*"x^-1", "-x^-1",*/
            "x", "-x", /*"x^-1", "-x^-1",*/
            "25x", "-32x",
            "x^42", "-x^13", /*"x^-91", "-x^-31",*/
            "12x^31", "-65x^32", /*"65x^-76", "-435x^-234",*/
            "12", "12", "0", "0", "1", "-2", "0", "0", "0", "1",
            "5", "2x^2"
    );

    @Test
    void testCreationFromString() {
        Monomial monomial;
        int i = 0;
        for (String m : monomials) {
            monomial = new Monomial(m);
            Assertions.assertEquals(expectedMonomial.get(i++), monomial);
        }
    }

    @Test
    void testToString() {
        Monomial monomial;
        int i = 0;
        for (String m : monomials) {
            monomial = new Monomial(m);
            Assertions.assertEquals(expectedString.get(i++), monomial.toString());
        }
    }

    @Test
    void isZero() {
        Assertions.assertTrue(new Monomial("0").isZero());
        Assertions.assertTrue(new Monomial("0x").isZero());
        Assertions.assertTrue(new Monomial("0x^2").isZero());
        Assertions.assertTrue(new Monomial("0x^0").isZero());
        Assertions.assertTrue(new Monomial("-0").isZero());
        Assertions.assertTrue(new Monomial("-0x").isZero());
        Assertions.assertTrue(new Monomial("-0x^2").isZero());
        Assertions.assertTrue(new Monomial("-0x^0").isZero());
        Assertions.assertTrue(new Monomial("+0").isZero());
        Assertions.assertTrue(new Monomial("+0x").isZero());
        Assertions.assertTrue(new Monomial("+0x^2").isZero());
        Assertions.assertTrue(new Monomial("+0x^0").isZero());
        Assertions.assertFalse(new Monomial("x").isZero());
        Assertions.assertFalse(new Monomial("x^0").isZero());
        Assertions.assertFalse(new Monomial("x^0").isZero());
    }
}
