package com.katonaaron.calc.util;

import com.katonaaron.calc.exception.DivisionByZeroMonomialException;
import com.katonaaron.calc.model.Monomial;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class MonomialsTest {

    @Test
    void add() {
        Monomial m1 = new Monomial("2x^2");
        Monomial m2 = new Monomial("3x^2");
        Assertions.assertEquals(new Monomial("5x^2"), Monomials.add(m1, m2));
        Assertions.assertTrue(Monomials.add(m1, new Monomial("-2x^2")).isZero());
        Assertions.assertEquals(m1, Monomials.add(m1, new Monomial(0.0, 2)));
    }

    @Test
    void subtract() {
        Monomial m1 = new Monomial("2x^2");
        Monomial m2 = new Monomial("3x^2");
        Assertions.assertEquals(new Monomial("-x^2"), Monomials.subtract(m1, m2));
        Assertions.assertTrue(Monomials.subtract(m1, new Monomial("2x^2")).isZero());
        Assertions.assertEquals(m1, Monomials.subtract(m1, new Monomial(0.0, 2)));
    }

    @Test
    void multiply() {
        Monomial m1 = new Monomial("2x");
        Monomial m2 = new Monomial("-3x^3");
        Assertions.assertEquals(new Monomial("-6x^4"), Monomials.multiply(m1, m2));
        Assertions.assertTrue(Monomials.multiply(m1, new Monomial("0")).isZero());
        Assertions.assertEquals(m1, Monomials.multiply(m1, new Monomial("1")));
    }

    @Test
    void divide() {
        Monomial m1 = new Monomial("6x^9");
        Monomial m2 = new Monomial("-3x^3");
        Assertions.assertEquals(new Monomial("-2x^6"), Monomials.divide(m1, m2));
        Assertions.assertEquals(m1, Monomials.divide(m1, new Monomial("1")));
        Assertions.assertEquals(new Monomial("1"), Monomials.divide(m1, m1));
        Assertions.assertThrows(DivisionByZeroMonomialException.class, () -> Monomials.divide(m1, new Monomial("0")));
    }

    @Test
    void differentiate() {
        Monomial m1 = new Monomial("3x^2");
        Monomial m2 = new Monomial("1");
        Assertions.assertTrue(Monomials.differentiate(new Monomial("0")).isZero());
        Assertions.assertTrue(Monomials.differentiate(m2).isZero());
        Assertions.assertEquals(new Monomial("6x"), Monomials.differentiate(m1));
    }

    @Test
    void integrate() {
        Monomial m1 = new Monomial("3x^2");
        Monomial m2 = new Monomial("1");
        Assertions.assertTrue(Monomials.integrate(new Monomial("0")).isZero());
        Assertions.assertEquals(new Monomial("x"), Monomials.integrate(m2));
        Assertions.assertEquals(new Monomial("x^3"), Monomials.integrate(m1));
    }

    @Test
    void negate() {
        Monomial m1 = new Monomial("0");
        Monomial m2 = new Monomial("2x");
        Assertions.assertTrue(Monomials.negate(m1).isZero());
        Assertions.assertEquals(new Monomial("-2x"), Monomials.negate(m2));
    }
}
