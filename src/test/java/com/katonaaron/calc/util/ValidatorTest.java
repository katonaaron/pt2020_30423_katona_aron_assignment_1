package com.katonaaron.calc.util;

import com.katonaaron.calc.exception.InvalidMonomialFormatException;
import com.katonaaron.calc.exception.InvalidPolynomialFormatException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

class ValidatorTest {

    public static final List<String> validMonomials = List.of(
            "x", "-x", "1x", "-1x",
            "x^1", "-x^1",
            "1x^1", "-1x^1",
            "25x", "-32x",
            "x^42", "-x^13",
            "12x^31", "-65x^32",
            "12", "12x^0", "0", "0x^0", "x^0", "-0", "-0x", "-0x^2", "1x^0",
            "+5", "+2x^+2"
    );

    public static final List<String> invalidMonomials = List.of(
            "x^-1", "-x^-1", "1x^-1", "-1x^-1", "x^-91", "-x^-31", "65x^-76", "-435x^-234", "-2x^-0",
            "2x^-", "2x^-0", "", "-+x", "xasd"
    );

    public static final List<String> validPolynomials = List.of(
            "1+x", "x^2+x+1", "-x+x-2"
    );

    public static final List<String> invalidPolynomials = List.of(
            "", "1*x", "x^-2", "x^", "-+1"
    );


    @Test
    void validatePolynomial() {
        for (String m : validMonomials) {
            Assertions.assertDoesNotThrow(() -> Validator.validatePolynomial(m));
        }

        for (String m : invalidMonomials) {
            Assertions.assertThrows(InvalidPolynomialFormatException.class, () -> Validator.validatePolynomial(m));
        }

        for (String m : validPolynomials) {
            Assertions.assertDoesNotThrow(() -> Validator.validatePolynomial(m));
        }

        for (String m : invalidPolynomials) {
            Assertions.assertThrows(InvalidPolynomialFormatException.class, () -> Validator.validatePolynomial(m));
        }
    }


    @Test
    void validateMonomial() {
        for (String m : validMonomials) {
            Assertions.assertDoesNotThrow(() -> Validator.validateMonomial(m));
        }

        for (String m : invalidMonomials) {
            Assertions.assertThrows(InvalidMonomialFormatException.class, () -> Validator.validateMonomial(m));
        }

    }
}
