package com.katonaaron.calc.util;

import com.katonaaron.calc.exception.DivisionByZeroPolynomialException;
import com.katonaaron.calc.model.Polynomial;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

class PolynomialsTest {

    @Test
    void add() {
        Polynomial p1 = new Polynomial("x^2 + 3x + 5");
        Polynomial p2 = new Polynomial("x + 1");
        Assertions.assertEquals(new Polynomial("x^2+4x+6"), Polynomials.add(p1, p2));
        Assertions.assertTrue(Polynomials.add(p1, new Polynomial("-x^2 - 3x - 5")).isZero());
        Assertions.assertEquals(p1, Polynomials.add(p1, new Polynomial("0")));
    }

    @Test
    void divide() {
        Polynomial p1 = new Polynomial("x^2 + 3x + 5");
        Polynomial p2 = new Polynomial("x + 1");
        Assertions.assertEquals(List.of(new Polynomial("x+2"), new Polynomial("3")), Polynomials.divide(p1, p2));
        Assertions.assertEquals(List.of(p1, new Polynomial("")), Polynomials.divide(p1, new Polynomial("1")));
        Assertions.assertEquals(List.of(new Polynomial(""), p1), Polynomials.divide(p1, new Polynomial("x^3")));
        Assertions.assertEquals(List.of(new Polynomial("1"), new Polynomial("")), Polynomials.divide(p1, p1));
        Assertions.assertThrows(DivisionByZeroPolynomialException.class, () -> Polynomials.divide(p1, new Polynomial("")));
    }

    @Test
    void subtract() {
        Polynomial p1 = new Polynomial("x^2 + 3x + 5");
        Polynomial p2 = new Polynomial("x + 1");
        Assertions.assertTrue(Polynomials.subtract(p1, new Polynomial("x^2 + 3x + 5")).isZero());
        Assertions.assertEquals(p1, Polynomials.subtract(p1, new Polynomial("0")));
        Assertions.assertEquals(new Polynomial("x^2 + 2x +4"), Polynomials.subtract(p1, p2));
    }

    @Test
    void multiply() {

        Polynomial p1 = new Polynomial("x^2 + 3x + 5");
        Polynomial p2 = new Polynomial("x + 1");
        Assertions.assertTrue(Polynomials.multiply(p1, new Polynomial("0")).isZero());
        Assertions.assertEquals(p1, Polynomials.multiply(p1, new Polynomial("1")));
        Assertions.assertEquals(new Polynomial("x^3+4x^2+8x+5"), Polynomials.multiply(p1, p2));
    }

    @Test
    void differentiate() {
        Polynomial p1 = new Polynomial("3x^2 + 6x + 5");
        Polynomial p2 = new Polynomial("1");
        Assertions.assertTrue(Polynomials.differentiate(new Polynomial("0")).isZero());
        Assertions.assertTrue(Polynomials.differentiate(p2).isZero());
        Assertions.assertEquals(new Polynomial("6x + 6"), Polynomials.differentiate(p1));
    }

    @Test
    void integrate() {
        Polynomial p1 = new Polynomial("3x^2 + 6x + 5");
        Polynomial p2 = new Polynomial("1");
        Assertions.assertTrue(Polynomials.integrate(new Polynomial("0")).isZero());
        Assertions.assertEquals(new Polynomial("x"), Polynomials.integrate(p2));
        Assertions.assertEquals(new Polynomial("x^3 + 3x^2 + 5x"), Polynomials.integrate(p1));
    }
}
