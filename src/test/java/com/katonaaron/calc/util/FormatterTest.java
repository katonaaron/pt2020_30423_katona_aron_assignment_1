package com.katonaaron.calc.util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class FormatterTest {

    @Test
    void doubleToString() {
        Assertions.assertEquals("12", Formatter.doubleToString(12.000));
        Assertions.assertEquals("12.000001", Formatter.doubleToString(12.000001));
        Assertions.assertEquals("12.001", Formatter.doubleToString(12.00100));
        Assertions.assertEquals("0", Formatter.doubleToString(0));
        Assertions.assertEquals("1.2345", Formatter.doubleToString(1.23450000));
    }
}
